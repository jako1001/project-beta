from django.urls import path

from .views import (
    api_salespeople_list,
    api_customers_list,
    api_salesrecord_list,
)

urlpatterns = [
    path("salespeople/", api_salespeople_list, name="api_salespeople_list"),
    path("customers/", api_customers_list, name="api_customers_list"),
    path("salesrecords/", api_salesrecord_list, name="api_salesrecord_list"),
]
