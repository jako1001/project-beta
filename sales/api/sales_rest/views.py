from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import AutomobileVO, Salesperson, Customer, SalesRecord
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SalesrecordEncoder
)


@require_http_methods(["GET", "POST"])
def api_salespeople_list(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesPeople": salesperson}, encoder=SalespersonEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson, encoder=SalespersonEncoder, safe=False
            )
        except:
            return JsonResponse(
                {"error": "Failed to create salesperson"}, status=400
            )


@require_http_methods(["GET", "POST"])
def api_customers_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except:
            return JsonResponse(
                {"error": "Failed to create customer"}, status=400
            )


@require_http_methods(["GET", "POST"])
def api_salesrecord_list(request):
    if request.method == "GET":
        salesrecords = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": salesrecords}, encoder=SalesrecordEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            content = {
                "automobile": AutomobileVO.objects.get(
                    vin=content["automobile"]
                ),
                "salesperson": Salesperson.objects.get(
                    employee_number=content["salesperson"]
                ),
                "customer": Customer.objects.get(id=content["customers"]),
                "price": content["price"],
            }
            salesrecord = SalesRecord.objects.create(**content)
            return JsonResponse(
                {"salesrecord": salesrecord},
                encoder=SalesrecordEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {
                    "error": "Failed to create sale record, automobile does not exist in inventory"
                },
                status=400,
            )
