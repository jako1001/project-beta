import { Link } from "react-router-dom";
import { useState } from "react";

function Nav() {
  const [openSales, setOpenSales] = useState();
  const [openInventory, setOpenInventory] = useState();

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          CarCar
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse"
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                onClick={() => setOpenInventory(openInventory)}
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <Link className="dropdown-item" to="/automobiles/">
                    View automobiles
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/automobiles/new/">
                    Add new automobile
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/manufacturers/">
                    View manufacturers
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/manufacturers/new/">
                    Add new manufacturer
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/models/">
                    View models
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/models/new/">
                    Add new vehicle model
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                onClick={() => setOpenSales(openSales)}
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <Link className="dropdown-item" to="/customers/">
                    Customer List
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/customers/new/">
                    New Customer Form
                  </Link>
                  <div className="dropdown-divider"></div>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salespersons/">
                    Sales Staff List
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salespersons/new/">
                    New Salesperson Form
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    className="dropdown-item"
                    to="/salespersons/search/"
                  >
                    Search Sales Staff
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salesrecords/">
                    List of All Sales
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    className="dropdown-item"
                    to="/salesrecords/search/"
                  >
                    Search All Sales
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/salesrecords/new/">
                    New Sale Form
                  </Link>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <Link className="dropdown-item" to="/technicians/">
                    View Technicians
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/technicians/new/">
                    Add new technician
                  </Link>
                </li>
                <div className="dropdown-divider"></div>
                <li className="nav-item">
                  <Link
                    className="dropdown-item"
                    to="/appointments/search/"
                  >
                    Search all appointments
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/appointments/">
                    View active appointments
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="dropdown-item" to="/appointments/new">
                    Add new appointment
                  </Link>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
