import { useEffect, useState } from "react";

function SalesRecordSearch() {
  const [sales_people, setSalesPeopleData] = useState([]);
  const [sales_records, setSalesRecordData] = useState([]);
  const [filterValue, setFilterValue] = useState("");
  const [filterStaff, setFilterStaff] = useState("");

  const getAutomobileData = async () => {
    const response = await fetch("http://localhost:8090/api/salesrecords/");
    if (response.ok) {
      const data = await response.json();
      const salesRecordData = data.sales_records.map((record) => {
        return {
          salesperson_name: record.salesperson.salesperson_name,
          vin: record.vin,
          customer: record.customer.customer_name,
          price: record.price,
        };
      });
      setSalesRecordData(salesRecordData);
    }
  };

  const getSalespersonData = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      const salesPersonData = data.salesPeople.map((person) => {
        return {
          id: person.id,
          salesperson_name: person.salesperson_name,
          employee_number: person.employee_number,
        };
      });
      setSalesPeopleData(salesPersonData);
    }
  };

  useEffect(() => {
    getAutomobileData();
    getSalespersonData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterValue(e.target.value.toLowerCase());
  };

  const handleStaffFilter = (e) => {
    setFilterStaff(e.target.options[e.target.value].text);
  };

  return (
    <>
      <p />
      <h1>Search Sales Records</h1>
      <div className="mb-3">
        <select
          onChange={handleStaffFilter}
          required
          name="sales_people"
          id="sales_people"
          className="form-select"
        >
          <option value="">Select a Salesperson</option>
          {sales_people &&
            sales_people.map((person) => {
              return (
                <option key={person.id} value={person.id}>
                  {person.salesperson_name}
                </option>
              );
            })}
        </select>
      </div>
      <div className="form-outline">
        <input
          onChange={handleFilterChange}
          type="search"
          id="form1"
          className="form-control"
          placeholder="Type an employee name"
          aria-label="Search"
        />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee Name</th>
            <th>VIN</th>
            <th>Customer</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales_records
            .filter((record) =>
              record.salesperson_name.toLowerCase().includes(filterValue)
            )
            .filter((record) =>
              record.salesperson_name
                .toLowerCase()
                .includes(filterStaff.toLowerCase())
            )
            .map((record) => {
              return (
                <tr key={record.id}>
                  <td>{record.salesperson_name}</td>
                  <td>{record.vin}</td>
                  <td>{record.customer}</td>
                  <td>{record.price}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
}

export default SalesRecordSearch;
