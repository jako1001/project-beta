import React, { useState, useEffect } from "react";

const SalesRecordForm = () => {
  const [automobile, setAutomobileData] = useState([]);
  const [salesperson, setSalespersonData] = useState([]);
  const [customers, setCustomerData] = useState([]);
  const [price, setPriceData] = useState("");
  const [selectedAutomobile, setSelectedAutomobile] = useState("");
  const [selectedSalesperson, setSelectedSalesperson] = useState("");
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [salesRecords, setSalesRecordData] = useState([]);

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setSelectedCustomer(value);
  };

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);
  };

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setSelectedAutomobile(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPriceData(value);
  };

  const getCustomer = async () => {
    const customerUrl = "http://localhost:8090/api/customers";
    const response = await fetch(customerUrl);
    if (response.ok) {
      const data = await response.json();
      setCustomerData(data.customers);
    }
  };

  const getSalesperson = async () => {
    const salespersonUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespersonUrl);
    if (response.ok) {
      const data = await response.json();
      setSalespersonData(data.salesPeople);
    }
  };

  const getAutomobile = async () => {
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobileUrl);
    if (response.ok) {
      const auto_data = await response.json();
      setAutomobileData(auto_data.autos);
    }
  };

  const getSalesRecord = async () => {
    const salesrecordUrl = "http://localhost:8090/api/salesrecords/";
    const response = await fetch(salesrecordUrl);
    if (response.ok) {
      const data = await response.json();
      setSalesRecordData(data.sales_records);
    }
  };

  const unsoldAutos = automobile.filter((auto) => {
    const soldAutos = salesRecords.some(
      (salesRecords) => salesRecords.automobile.vin === auto.vin
    );
    return !soldAutos;
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.automobile = selectedAutomobile;
    data.salesperson = selectedSalesperson;
    data.customers = selectedCustomer;
    data.price = price;
    const salesrecordUrl = "http://localhost:8090/api/salesrecords/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesrecordUrl, fetchConfig);
    if (response.ok) {
      setSelectedAutomobile("");
      setSelectedSalesperson("");
      setSelectedCustomer("");
      setPriceData("");
      alert("Created new sale record!");
    } else {
      alert("Failed to create new sale record");
    }
  };

  useEffect(() => {
    getCustomer();
    getSalesperson();
    getAutomobile();
    getSalesRecord();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Post New Sale</h1>
          <form id="create-sale-form" onSubmit={handleSubmit}>
            <div className="mb-3">
              <select
                required
                name="automobile"
                id="automobile"
                className="form-select"
                onChange={handleAutomobileChange}
              >
                <option value="">Choose Automobile</option>
                {unsoldAutos.map((automobile) => (
                  <option key={automobile.vin} value={automobile.vin}>
                    {automobile.vin}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select
                required
                name="sales_person"
                id="sales_person"
                className="form-select"
                onChange={handleSalespersonChange}
              >
                <option value="">Choose Salesperson</option>
                {salesperson &&
                  salesperson.map((person) => (
                    <option key={person.id} value={person.id}>
                      {person.salesperson_name}
                    </option>
                  ))}
              </select>
            </div>
            <div className="mb-3">
              <select
                required
                name="customer_name"
                id="customer_name"
                className="form-select"
                onChange={handleCustomerChange}
              >
                <option value="">Choose Customer</option>
                {customers &&
                  customers.map((customer) => (
                    <option key={customer.id} value={customer.id}>
                      {customer.customer_name}
                    </option>
                  ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="price"
                onChange={handlePriceChange}
                required
                type="text"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Complete Sale</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SalesRecordForm;
