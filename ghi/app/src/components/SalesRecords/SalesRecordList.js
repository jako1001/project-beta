import { useEffect, useState } from "react";

const SalesRecordList = () => {
  const [sales_records, setSalesRecordData] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/salesrecords/");
    if (response.ok) {
      const data = await response.json();
      setSalesRecordData(data.sales_records);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperon</th>
          <th>Employee Number</th>
          <th>Price</th>
          <th>Automobile</th>
          <th>Customer</th>
        </tr>
      </thead>
      <tbody>
        {sales_records &&
          sales_records.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.salesperson_name}</td>
                <td>{sale.salesperson.employee_number}</td>
                <td>{sale.price}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.customer.customer_name}</td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default SalesRecordList;
