import React, { useState } from "react";

const TechnicianForm = () => {
  const [name, setName] = useState("");
  const [employee_number, setEmployeeNumber] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.employee_number = employee_number;

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfigs = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(technicianUrl, fetchConfigs);
    if (resp.ok) {
      setName("");
      setEmployeeNumber("");
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  };

  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new technician</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                id="name"
                className="form-control"
                name="name"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={employee_number}
                onChange={handleEmployeeNumberChange}
                placeholder="employee_number"
                required
                type="text"
                id="employee_number"
                className="form-control"
                name="employee_number"
              />
              <label htmlFor="employee_number">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default TechnicianForm;
