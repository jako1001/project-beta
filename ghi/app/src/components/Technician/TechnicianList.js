import { useState, useEffect } from "react";

const TechnicianList = () => {
  const [technicians, setTechnician] = useState([]);

  const getData = async () => {
    const resp = await fetch("http://localhost:8080/api/technicians/");
    if (resp.ok) {
      const data = await resp.json();
      setTechnician(data.technicians);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const url = `http://localhost:8080/api/technicians/${e.target.id}`;

    const fetchConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(url, fetchConfigs);
    const data = await resp.json();

    setTechnician(
      technicians.filter(
        (technician) => String(technician.id) !== e.target.id
      )
    );
  };

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Our Technicians</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Employee ID</th>
                </tr>
              </thead>

              <tbody>
                {technicians.map((technician) => {
                  return (
                    <tr key={technician.id}>
                      <td>{technician.name}</td>
                      <td>{technician.employee_number}</td>
                      {/* <td><Link to={`/technicians/${technician.id}`}>{ technician.name }</Link></td> */}
                      <td>
                        <button
                          onClick={handleDelete}
                          id={technician.id}
                          className="btn btn-danger"
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default TechnicianList;
