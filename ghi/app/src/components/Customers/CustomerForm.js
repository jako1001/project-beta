import React, { useState } from "react";

const CustomerForm = () => {
  const [customer_name, setCustomerNameData] = useState("");

  const [address, setCustomerAddress] = useState("");

  const [phone_number, setCustomerPhoneNumber] = useState("");

  const handleNameChange = (event) => {
    const value = event.target.value;
    setCustomerNameData(value);
  };

  const handleAddressChange = (event) => {
    const value = event.target.value;
    setCustomerAddress(value);
  };

  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setCustomerPhoneNumber(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.customer_name = customer_name;
    data.address = address;
    data.phone_number = phone_number;
    const customerUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    
    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      setCustomerNameData("");
      setCustomerAddress("");
      setCustomerPhoneNumber("");
      alert("Created new customer");
    } else {
      alert("Failed to create new customer");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="customer_name"
                required
                type="text"
                name="customer_name"
                id="customer_name"
                className="form-control"
              />
              <label htmlFor="customer_name">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleAddressChange}
                placeholder="address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
              />
              <label htmlFor="address">Customer Address</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePhoneNumberChange}
                placeholder="phone_number"
                required
                type="text"
                name="phone_number"
                id="phone_numberr"
                className="form-control"
              />
              <label htmlFor="phone_number">Customer Phone Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CustomerForm;
