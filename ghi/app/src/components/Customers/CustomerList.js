import { useEffect, useState } from "react";

const CustomerList = () => {
  const [customerData, setCustomerData] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomerData(data.customers);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const id = e.target.id;
    const customerUrl = `http://localhost:8090/api/customers/${id}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(customerUrl, fetchConfig);

    if (response.ok) {
      getData();
    } else {
      alert("Customer was not found!");
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Customer Name</th>
          <th>Address</th>
          <th>Phone Number</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {customerData &&
          customerData.map((cust) => {
            return (
              <tr key={cust.id}>
                <td>{cust.customer_name}</td>
                <td>{cust.address}</td>
                <td>{cust.phone_number}</td>
                <td>
                  <button
                    className="btn btn-med btn-primary"
                    onClick={() => handleDelete(cust.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default CustomerList;
