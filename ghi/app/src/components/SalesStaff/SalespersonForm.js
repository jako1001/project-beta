import React, { useState } from "react";

const SalespersonForm = () => {
  const [salesperson_name, setSalespersonNameData] = useState("");
  const [employee_number, setEmployeeNumber] = useState("");
  const handleNameChange = (event) => {
    const value = event.target.value;
    setSalespersonNameData(value);
  };
  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.salesperson_name = salesperson_name;
    data.employee_number = employee_number;
    const salespersonUrl = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      setSalespersonNameData("");
      setEmployeeNumber("");
      alert("Created new salesperson");
    } else {
      alert("Failed to create new salesperson");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salesperon-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="salesperson_name"
                required
                type="text"
                name="salesperson_name"
                id="salesperson_name"
                className="form-control"
              />
              <label htmlFor="salesperson_name">Salesperson Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeNumberChange}
                placeholder="employee_number"
                required
                type="number"
                name="employee_number"
                id="employee_number"
                className="form-control"
              />
              <label htmlFor="employee_number">Employee Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SalespersonForm;
