import React, { useState, useEffect } from "react";

const SalespersonSearch = () => {
  const [salesperson, setSalesperson] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getData = async () => {
    const resp = await fetch("http://localhost:8090/api/salespeople/");
    if (resp.ok) {
      const data = await resp.json();
      const salespersonData = data.salesPeople.map((person) => {
        return {
          id: person.id,
          name: person.salesperson_name,
          employee_number: person.employee_number,
        };
      });
      setSalesperson(salespersonData);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  return (
    <>
      <p />
      <h1>Search for a sales staff member</h1>
      <div className="form-outline">
        <input
          onChange={handleFilterChange}
          type="search"
          id="form1"
          className="form-control"
          placeholder="Type an employee name"
          aria-label="Search"
        />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee Name</th>
            <th>Employee Number</th>
          </tr>
        </thead>
        <tbody>
          {salesperson
            .filter((person) =>
              person.name.toLowerCase().includes(filterTerm)
            )
            .map((person) => {
              return (
                <tr key={person.id}>
                  <td>{person.name}</td>
                  <td>{person.employee_number}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
};

export default SalespersonSearch;
