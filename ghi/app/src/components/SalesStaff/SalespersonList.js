import React, { useEffect, useState } from "react";

const SalespersonList = () => {
  const [salespersonData, setSalespersonData] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalespersonData(data.salesPeople);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const id = e.target.id;
    const salespeopleUrl = `http://localhost:8090/api/salespeople/${id}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      getData();
    } else {
      alert("Staff member was not found!");
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson Name</th>
          <th>Employee Number</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {salespersonData &&
          salespersonData.map((person) => {
            return (
              <tr key={person.id}>
                <td>{person.salesperson_name}</td>
                <td>{person.employee_number}</td>

                <td>
                  <button
                    className="btn btn-med btn-primary"
                    onClick={() => handleDelete(person.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default SalespersonList;
