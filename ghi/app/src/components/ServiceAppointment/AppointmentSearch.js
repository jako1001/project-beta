import React, { useState, useEffect } from "react";

const ServiceAppointmentSearch = () => {
  const [appointments, setAppointment] = useState([]);
  const [filterTerm, setFilterTerm] = useState("");

  const getData = async () => {
    const resp = await fetch("http://localhost:8080/api/appointments/");
    if (resp.ok) {
      const data = await resp.json();

      const appointmentData = data.appointments.map((appt) => {
        return {
          id: appt.id,
          vin: appt.vin,
          ownersName: appt.owners_name,
          date: new Date(appt.date).toLocaleDateString(),
          time: appt.time,
          technicianName: appt.assigned_technician.name,
          reasonForService: appt.reason_for_service,
          isCompleted: appt.is_completed,
          isVip: appt.is_vip,
        };
      });

      setAppointment(appointmentData);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toLowerCase());
  };

  return (
    <>
      <p />
      <h1>Search for a service appointment</h1>
      <div className="form-outline">
        <input onChange={handleFilterChange} placeholder="Type a VIN" />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason for Service</th>
            <th>Completed?</th>
            <th>Vip?</th>
          </tr>
        </thead>

        <tbody>
          {appointments
            .filter((appt) => appt.vin.toLowerCase().includes(filterTerm))
            .map((appt) => {
              return (
                <tr key={appt.id}>
                  <td>{appt.vin}</td>
                  <td>{appt.ownersName}</td>
                  <td>{appt.date}</td>
                  <td>{appt.time}</td>
                  <td>{appt.technicianName}</td>
                  <td>{appt.reasonForService}</td>
                  <td>{appt.isCompleted}</td>
                  <td>{appt.isVip}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
};

export default ServiceAppointmentSearch;
