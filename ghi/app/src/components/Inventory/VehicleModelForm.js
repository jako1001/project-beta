import React, { useEffect, useState } from "react";

const VehicleModelForm = () => {
  const [name, setName] = useState("");
  const [picture_url, setPicture] = useState("");
  const [manufacturer_id, setManufacturer] = useState("");
  const [manufacturer, setManufacturers] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.picture_url = picture_url;
    data.manufacturer_id = manufacturer_id;
    data.manufacturer = manufacturer;
    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfigs = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const resp = await fetch(modelUrl, fetchConfigs);
    if (resp.ok) {
      setName("");
      setPicture("");
      setManufacturer("");
      alert("Created new Vehicle Model")
    } else {
      alert("Failed to create new Vehicle Model")
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new model</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="name"
                required
                type="text"
                id="name"
                className="form-control"
                name="name"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={picture_url}
                onChange={handlePictureChange}
                placeholder="picture_url"
                required
                type="text"
                id="picture_url"
                className="form-control"
                name="picture_url"
              />
              <label htmlFor="name">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={manufacturer_id}
                onChange={handleManufacturerChange}
                required
                id="manufacturer_id"
                className="form-control"
                name="manufacturer_id"
              >
                <option value="">Choose a manufacturer</option>
                {manufacturer.map((manufacturer) => {
                  return (
                    <option
                      key={manufacturer.href}
                      value={manufacturer.id}
                    >
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default VehicleModelForm;
