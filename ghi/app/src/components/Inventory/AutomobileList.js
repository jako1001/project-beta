import React, { useEffect, useState } from "react";

const AutomobileList = () => {
  const [automobile, setAutomobileData] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setAutomobileData(data.autos);
    }
  };

  const handleDelete = async (vin) => {
    const automobileUrl = `http://localhost:8100/api/automobiles/${vin}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Year</th>
          <th>Color</th>
          <th>Model</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {automobile &&
          automobile.map((autos) => {
            return (
              <tr key={autos.id}>
                <td>{autos.vin}</td>
                <td>{autos.year}</td>
                <td>{autos.color}</td>
                <td>{autos.model.name}</td>
                <td>
                  <button
                    className="btn btn-med btn-primary"
                    onClick={() => handleDelete(autos.vin)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default AutomobileList;
