import { useState, useEffect } from "react";

const VehicleModelList = () => {
  const [models, setModel] = useState([]);

  const getData = async () => {
    const resp = await fetch("http://localhost:8100/api/models/");
    if (resp.ok) {
      const data = await resp.json();
      setModel(data.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const url = `http://localhost:8100/api/models/${e.target.id}`;
    const fetchConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const resp = await fetch(url, fetchConfigs);
    const data = await resp.json();
    setModel(models.filter((model) => String(model.id) !== e.target.id));
  };

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Models</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Manufacturer</th>
                </tr>
              </thead>
              <tbody>
                {models.map((model) => {
                  return (
                    <tr key={model.id}>
                      <td>{model.name}</td>
                      <td>{model.manufacturer.name}</td>
                      <td>
                        <img
                          src={model.picture_url}
                          width="100"
                          alt="carPicture"
                        />
                      </td>
                      <td>
                        <button
                          onClick={handleDelete}
                          id={model.id}
                          className="btn btn-danger"
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default VehicleModelList;
