import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianList from "./components/Technician/TechnicianList";
import TechnicianForm from "./components/Technician/TechnicianForm";
import ServiceAppointmentList from "./components/ServiceAppointment/AppointmentList";
import ServiceAppointmentForm from "./components/ServiceAppointment/AppointmentForm";
import ServiceAppointmentSearch from "./components/ServiceAppointment/AppointmentSearch";
import CustomerList from "./components/Customers/CustomerList";
import CustomerForm from "./components/Customers/CustomerForm";
import SalespersonList from "./components/SalesStaff/SalespersonList";
import SalespersonForm from "./components/SalesStaff/SalespersonForm";
import SalespersonSearch from "./components/SalesStaff/SalespersonSearch";
import SalesRecordList from "./components/SalesRecords/SalesRecordList";
import SalesRecordSearch from "./components/SalesRecords/SalesRecordSearch";
import ManufacturerList from "./components/Inventory/ManufacturerList";
import ManufacturerForm from "./components/Inventory/ManufacturerForm";
import VehicleModelList from "./components/Inventory/VehicleModelList";
import VehicleModelForm from "./components/Inventory/VehicleModelForm";
import SalesRecordForm from "./components/SalesRecords/SalesRecordForm";
import AutomobileList from "./components/Inventory/AutomobileList";
import AutomobileForm from "./components/Inventory/AutomobileForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<ServiceAppointmentList />} />
            <Route path="new" element={<ServiceAppointmentForm />} />
            <Route path="search" element={<ServiceAppointmentSearch />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="salespersons">
            <Route index element={<SalespersonList />} />
            <Route path="new" element={<SalespersonForm />} />
            <Route path="search" element={<SalespersonSearch />} />
          </Route>
          <Route path="salesrecords">
            <Route index element={<SalesRecordList />} />
            <Route path="new" element={<SalesRecordForm />} />
            <Route path="search" element={<SalesRecordSearch />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
